<?php
namespace app\index\model;
use think\Model;
use app\admin\model\Exams;
class StuAnswers extends Model
{
	//设置主键
	protected $pk = 'id';
	//设置自动时间戳
	protected $createTime = 'c_time';
	//设置自动转换类型
	protected $type = [
		'sid'=> 'integer',
		'sid'=> 'integer',
		'total'=> 'integer',
		//'kejie'=> 'float',
		//'birthday' => 'datetime',
		//'info'=> 'array',
	];
	//获取器
	public function getAnswersAttr($value)
    {
        //$status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return json_decode($value,true);
    }
    public function getScoreAttr($value)
    {
        //$status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return json_decode($value,true);
    }
	//多对一关联 
	public function getexams()
    {
        return $this->belongsTo('app\admin\model\Exams','eid')->field('eid,title,subject,begintime,qids,description,timelong,allocate');
    }
    public function getstudent()
    {
        return $this->belongsTo('app\admin\model\Students','sid')->field('sid,snumber,truename,grade,class');
    }
	



}