<?php
namespace app\index\controller;
use think\Controller;
use think\Url;
use think\Request;
use app\admin\model\Questions;
use app\admin\model\Exams;
use app\index\model\StuAnswers;
use think\Db;

class Index extends Siku
{
	
    public function index()
    {
        //echo time();
        //$request = request();
        //Url::root('/index.php');
        //return APP_PATH.ROOT_PATH;
        //echo session('cid');
        $exams=db('exams')->where('find_in_set(:cid,cids)',['cid'=>session('cid')])->order('eid desc')->select();
        $this->assign('exams',$exams);
        //dump($exams);
        return $this->fetch();
        //return $request->domain();
      
    }
    public function check_paper(){
    	$eid=request()->param('eid');
    	//return $eid;
    	$edata=Exams::get($eid)->toArray();
    	if (time()<$edata['begintime']) {
    		return '还没有到考试时间！<br>'.'考试时间为'.date('Y-m-d H:i:s',$edata['begintime']).'<br>现在时间为'.date('Y-m-d H:i:s',time());
    	}elseif (time()>$edata['begintime']+($edata['timelong']*60)) {
    		return'该考试已经结束！';
    	}elseif ($edata['examstatus']==1) {
    		return '该考试尚未发布，请联系老师！';
    	}else{
    		return 1;
    	}
    	
    }
    public function paper(){
    	$eid=request()->param('eid');
    	//return $eid;
    	$edata=Exams::get($eid)->toArray();
        $stuannum=db('stu_answers')->where('sid',session('sid'))->where('eid',$eid)->count();
        if (time()<$edata['begintime']) {
            $this->error('还没有到考试时间！<br>'.'考试时间为'.date('Y-m-d H:i:s',$edata['begintime']).'<br>现在时间为'.date('Y-m-d H:i:s',time()));
        }elseif (time()>$edata['begintime']+($edata['timelong']*60)) {
            $this->error('该考试已经结束！');
        }elseif ($edata['examstatus']==1) {
            $this->error('该考试尚未发布，请联系老师！') ;
        }elseif ($stuannum >= $edata['subnum']) {
           
            $this->error('该考试只允许参考'.$edata['subnum'].'次！') ;
        }else{

            $list = new Questions;
            $with['getanswers'] = function ($query) {
                $query->order(['aid' => 'asc']);
            };
            $qdata =$list->with($with)->where('qid','in',$edata['qids'])->order('sort asc')->select();
            $endtime=$edata['begintime']+($edata['timelong']*60);
            //echo (date("Y/m/d H:m:s",1584351000));
            // dump (floor($qdata[2]['sort']));
            $this->assign('endtime',$endtime);
            $this->assign('edata',$edata);
            $this->assign('qdata',$qdata);
            return $this->fetch();
        }
    	
    }
    public function do_answer(){
    	$formdata = request()->post();
    	if (!array_key_exists('answer', $formdata)) {
    		return '请完成考试，不能交白卷！';
    	}else{
            //dump($formdata);
    		$qids=db('exams')->where('eid',$formdata['eid'])->field('qids,subnum')->find();
			$questions=db('questions')->where('qid','in',$qids['qids'])->where('qtype','neq',5)->field('qid,qtype,rightanswer,score')->select()->toArray();
			foreach ($questions as $key => $q) {

				switch ($q['qtype']) {
				 	case 1://单选赋分
				 		if (!array_key_exists($q['qid'], $formdata['answer'])) {
			 			$formdata['answer'][$q['qid']]='null';
			 			}//判断是否没做这道题
				 		if ($formdata['answer'][$q['qid']]==$q['rightanswer']) {
				 			$stuscore[$q['qid']]=$q['score'];
				 		} else {
				 			$stuscore[$q['qid']]=0;
				 		}
				 		break;
			 		case 2://多选赋分
			 			if (!array_key_exists($q['qid'], $formdata['answer'])) {
			 			$formdata['answer'][$q['qid']]=array('null');
			 			}//判断是否没做这道题；
        		 		$rightanswerarr=explode(',', $q['rightanswer']);
        		 		$arrjiao=array_intersect($formdata['answer'][$q['qid']], $rightanswerarr);
        		 		//$arrcha=array_diff($rightanswerarr,$formdata['answer'][$q['qid']]);
        		 		 //dump($formdata['answer']);
        		 		// dump($rightanswerarr);
        		 		// dump($arrjiao);
        		 		// dump($arrcha);
        		 		//dump(array_values($formdata['answer'][$q['qid']])==$rightanswerarr);
        		 		if (array_values($formdata['answer'][$q['qid']])==$rightanswerarr) {
        		 			$stuscore[$q['qid']]=$q['score'];
        		 		} elseif ($formdata['answer'][$q['qid']]==$arrjiao) {
        		 			$stuscore[$q['qid']]=($q['score']/2);
        		 			//dump($rightanswerarr);
        		 		}else {
        		 			$stuscore[$q['qid']]=0;
        		 		}
			 		break;
				 	default:
                        $stuscore[$q['qid']]='null';
				 	break;
				}
			}
			//dump($stuscore);
			$stuan           = new StuAnswers;
            $subnum=$stuan->where('eid', $formdata['eid'])->where('sid',session('sid'))->count();
            if ($subnum>=$qids['subnum']) {
                return '该考试只允许提交'.$qids['subnum'].'次！您已提交'.$subnum.'次！';
            }else{
                $stuan->sid     = session('sid');
                $stuan->eid    = $formdata['eid'];
                $stuan->answers = json_encode($formdata['answer']);
                $stuan->score = json_encode($stuscore);
                $stuan->total = array_sum($stuscore);
                
                $re=$stuan->save();
                //dump($formdata['answer']);
                if ($re>0) {
                    return 1;
                } else {
                    return '提交数据失败！请联系管理员！';
                }
                
                //$a=json_decode(json_encode($formdata['answer']),true);//加true返回数组否则返回对象
                //dump($a);
            }
			

    	}
		


    }
	public function pass(){
    	return $this->fetch();
    }
    function check_pass(){
        $pass=request()->post('pass','','md5');
        $where['sid']=session('sid');
        $tpass=db('students')->where($where)->value('pass');
        if ($pass!=$tpass) {
            $re=0;
        } else {
            $re=1;
        }
        
         return $re;
    }
    function do_pass(){
        $pass=request()->post('pass','','md5');
        $where['sid']=session('sid');
        $re=db('students')->where($where)->setField('pass',$pass);
        
        return $re;
    }
    public function history(){
    	$edata=new StuAnswers();
    	$elist=$edata->with('getexams')->where('sid',session('sid'))->order('id', 'desc')->select()->toArray();
    	//dump($elist);
    	$this->assign('elist',$elist);
    	return $this->fetch();
    }
    public function myexam(){
    	$id=request()->param('id');
    	//dump($eid);
    	$edata=new StuAnswers();
    	$myexam=$edata->with('getexams')->order('id', 'desc')->where('id',$id)->find()->toArray();
    	//dump($myexam);
        //echo time();
        if(time()<$myexam['getexams']['begintime']+$myexam['getexams']['timelong']*60){
            $this->error('本次考试尚未结束！待结束后查看详情！');
        }else{
            $list = new Questions;
            $with['getanswers'] = function ($query) {
                    $query->order(['aid' => 'asc']);
                };
            $qdata =$list->with($with)->where('qid','in',$myexam['getexams']['qids'])->where('qtype','neq',5)->order('sort asc')->select()->toArray();
            $this->assign('myexam',$myexam);
            $this->assign('qdata',$qdata);
            //dump($qdata);
            return $this->fetch();

        }
    	
    }
}

