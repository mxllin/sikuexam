<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Teachers;
use app\admin\controller\Siku;
class Teacher extends Siku
{
    
    public function index()
    {
        return $this->fetch();
    }
    

    public function tdata()
    {
        $tdata= new Teachers();
        $limit=request()->param('limit');
        $page=request()->param('page');
        $map['truename']=request()->param('uname');
        $map['grade']=request()->param('grade');
        $map = array_diff($map, array(null,'null','',' '));
        $count=$tdata->where($map)->count();
        $list=$tdata->page($page,$limit)->where($map)->order('tid', 'desc')->select();
        return myjson(0,'',$count,$list);
    }
    public function add()
    {
        
        return $this->fetch();
        
    }
    public function do_add(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata;
            //return json($data);
            $tdata= new Teachers();
            $check=$tdata->where('truename',$formdata['truename'])->find();
            if ($check) {
                $re=2;
            } else{
                $a=$tdata->allowField(true)->save($formdata);
                //dump(input('post.'));
                //dump($formdata);
                //$userId = Db::name('data')->strict(false)->insertGetId($formdata);
                if ($a===false) {
                    $re=0;
                } else {
                    $re=1;
                }
            }
            
            return $re;
        }else{
            return '非法操作！';
        }
    }
    public function delcheck(){
        $checkid=request()->param('id');
        $re=db('teachers')->delete($checkid);
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        
        return $res;
    }
    public function edit(){
        $id=request()->param('id');
        //echo $id;
        $re=Teachers::get($id);
        //dump($re);
        $this->assign('tdata',$re);
        return $this->fetch();
    }
     public function do_edit(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata['id'];
            if (!is_md5($formdata['data']['pass'])) {
                $formdata['data']['pass']=md5($formdata['data']['pass']);
            }
             $tdata= new Teachers;
             $a=$tdata->allowField(true)->where('tid', $formdata['id'])->update($formdata['data']);
            if ($a===false) {
                $re=0;
            } else {
                $re=1;
            }
            return $re;
        }
    }
    public function del(){
        $id=request()->param('id');
        $re=db('teachers')->delete($id);
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        return $res;
    }

























    
    
    
   
    public function showimg(){
        $data=request()->param('data');
        return $data['id'];
    }
    public function other()
    {
    	return '其他功能按需开发！';
    }

    public function def(){
        $tdata=new Tdata;
        $tol=$tdata->count();
        $num=$tdata->distinct(true)->field('listener')->select();
        $kemu=db('tdata')->field('subject as name,count(id) as value')->group('subject')->select();
        $grade=db('tdata')->field('grade as name,count(id) as value')->group('grade')->select();
        //dump(i_array_column($kemu, 'name'));
        $this->assign('grade',$grade);
        $this->assign('kemu',$kemu);
        $this->assign('tol',$tol);
        $this->assign('num',count($num));
    	return $this->fetch();
    }
    // public function tubiao(){
    //     return $this->fetch();
    // }
    public function tubiao(){
        //echo request()->post('listener');
        $tname=request()->post('listener');
        $starttime=request()->post('starttime');
        $endtime=request()->post('endtime');
        if (!empty($tname) && !empty($starttime) && !empty($endtime)) {
            $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (!empty($tname) && empty($starttime) && empty($endtime)) {
            $listbar=db('tdata')->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (empty($tname) && !empty($starttime) && !empty($endtime)) {
           $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->field('listener as name,count(id) as value')->group('listener')->select();
        }else{
            $listbar=db('tdata')->field('listener as name,count(id) as value')->group('listener')->select();
        }
        $this->assign('listbar',$listbar);
         return $this->fetch();

    }
}
