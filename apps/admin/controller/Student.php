<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Students;
use app\admin\controller\Siku;
class Student extends Siku
{
    
    public function index()
    {
        return $this->fetch();
    }
    

    public function stu_data()
    {
        $studata= new Students();
        $limit=request()->param('limit');
        $page=request()->param('page');
        $map['truename']=request()->param('truename');
        $map['class']=request()->param('class');
        $map = array_diff($map, array(null,'null','',' '));
        $count=$studata->where($map)->count();
        $list=$studata->page($page,$limit)->where($map)->with('getclass')->order('sid', 'desc')->select();
        return myjson(0,'',$count,$list);
    }
    public function add()
    {
        $classes=db('classes')->select();
        //dump($classes);
        $this->assign('classes',$classes);
        return $this->fetch();
        
    }
    public function do_add(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata;
            //return json($data);
            $tdata= new Students();
            $check=$tdata->where('truename',$formdata['truename'])->whereOr('snumber',$formdata['snumber'])->find();
            if ($check) {
                $re=2;
            } else{
                $formdata['pass']=md5($formdata['pass']);
                $a=$tdata->allowField(true)->save($formdata);
                //dump(input('post.'));
                //dump($formdata);
                //$userId = Db::name('data')->strict(false)->insertGetId($formdata);
                if ($a===false) {
                    $re=0;
                } else {
                    $re=1;
                }
            }
            
            return $re;
        }else{
            return '非法操作！';
        }
    }
    public function delcheck(){
        $checkid=request()->param('id');
        $re=db('students')->where('sid','in',$checkid)->delete();
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        
        return $res;
    }
    public function edit(){
        $classes=db('classes')->select();
        //dump($classes);
        $this->assign('classes',$classes);
        $id=request()->param('id');
        //echo $id;
        $re=Students::get($id);
        //dump($re);
        $this->assign('sdata',$re);
        return $this->fetch();
    }
     public function do_edit(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata['id'];
            if (!is_md5($formdata['data']['pass'])) {
                $formdata['data']['pass']=md5($formdata['data']['pass']);
            }
             $tdata= new Students;
             $a=$tdata->allowField(true)->where('sid', $formdata['sid'])->update($formdata['data']);
            if ($a===false) {
                $re=0;
            } else {
                $re=1;
            }
            return $re;
        }
    }
    public function del(){
        $sid=request()->param('sid');
        //echo $sid;
        $re=db('students')->where('sid',$sid)->delete();
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        return $res;
    }

























    
    
    
   
    public function showimg(){
        $data=request()->param('data');
        return $data['id'];
    }
    public function other()
    {
    	return '其他功能按需开发！';
    }

    public function def(){
        $tdata=new Tdata;
        $tol=$tdata->count();
        $num=$tdata->distinct(true)->field('listener')->select();
        $kemu=db('tdata')->field('subject as name,count(id) as value')->group('subject')->select();
        $grade=db('tdata')->field('grade as name,count(id) as value')->group('grade')->select();
        //dump(i_array_column($kemu, 'name'));
        $this->assign('grade',$grade);
        $this->assign('kemu',$kemu);
        $this->assign('tol',$tol);
        $this->assign('num',count($num));
    	return $this->fetch();
    }
    // public function tubiao(){
    //     return $this->fetch();
    // }
    public function tubiao(){
        //echo request()->post('listener');
        $tname=request()->post('listener');
        $starttime=request()->post('starttime');
        $endtime=request()->post('endtime');
        if (!empty($tname) && !empty($starttime) && !empty($endtime)) {
            $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (!empty($tname) && empty($starttime) && empty($endtime)) {
            $listbar=db('tdata')->where('listener','like','%'.$tname.'%')->field('listener as name,count(id) as value')->group('listener')->select();
        }elseif (empty($tname) && !empty($starttime) && !empty($endtime)) {
           $listbar=db('tdata')->whereTime('creat_time','between', [$starttime, $endtime])->field('listener as name,count(id) as value')->group('listener')->select();
        }else{
            $listbar=db('tdata')->field('listener as name,count(id) as value')->group('listener')->select();
        }
        $this->assign('listbar',$listbar);
         return $this->fetch();

    }
}
