<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Classes as BanjiModel;
use app\admin\model\Students;
use app\admin\controller\Siku;
class Banji extends Siku
{
    
    public function index()
    {
        
        return $this->fetch();
    }
    

    public function classdata()
    {
        $tdata= new BanjiModel();
        $limit=request()->param('limit');
        $page=request()->param('page');
        $map['classnumber']=request()->param('classnumber');
        //$map['grade']=request()->param('grade');
        $map = array_diff($map, array(null,'null','',' '));
        $count=$tdata->where($map)->count();
        $list=$tdata->page($page,$limit)->where($map)->withCount('getstudents')->order('cid', 'desc')->select();
        //$re=$list->toArray();
        //dump($re);
        return myjson(0,'',$count,$list);
    }
    public function add()
    {
        
        return $this->fetch();
        
    }
    public function do_add(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata;
            //return json($data);
            $tdata= new BanjiModel();
            $check=$tdata->where('title',$formdata['title'])->find();
            if ($check) {
                $re='已经存在此班级名称，请勿重复！';
            } else{
                $a=$tdata->allowField(true)->save($formdata);
                //dump(input('post.'));
                //dump($formdata);
                //$userId = Db::name('data')->strict(false)->insertGetId($formdata);
                if ($a===false) {
                    $re='数据错误！';
                } else {
                    $re=1;
                }
            }
            
            return $re;
        }else{
            return '非法操作！';
        }
    }
    public function delcheck(){
        $checkid=request()->param('id');
        $re=db('teachers')->delete($checkid);
        if ($re > 0) {
            $res=1;
        } else {
           $res=0;
        }
        
        return $res;
    }
    public function edit(){
        $id=request()->param('id');
        //echo $id;
        $re=BanjiModel::get($id);
        //dump($re);
        $this->assign('cdata',$re);
        return $this->fetch();
    }
     public function do_edit(){
        
        if (request()->isPost()) {
            $formdata = request()->post();
            //return $formdata['id'];
             $tdata= new BanjiModel;
             $a=$tdata->allowField(true)->where('cid', $formdata['cid'])->update($formdata['data']);
            if ($a===false) {
                $re='修改失败，请刷新重试';
            } else {
                $re=1;
            }
            return $re;
        }
    }
    public function del(){
        $cid=request()->param('id');
        $re=db('students')->where('cid',$cid)->delete();
        if ($re>0) {
            $res=db('classes')->delete($cid);
            if ($res>0) {
                return 1 ;
            } else {
                return '删除班级错误！';
            }
            
        } else {
            return '删除学生错误！';
        }
    }
    public function upstu(){
        $cid=request()->param('cid');
        //echo $cid;
        $banji=Db::name('classes')->where('cid',$cid)->find();
        $this->assign('banji',$banji);
         return $this->fetch();
    }
    public function import(){
        if(request()->isPost()){
            $cid=request()->param('cid');
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public' .DS.'uploads'. DS . 'excel');
            if($info){
                //获取文件所在目录名
                $path=ROOT_PATH . 'public' . DS.'uploads'.DS .'excel/'.$info->getSaveName();
                //加载PHPExcel类
                vendor("PHPExcel.PHPExcel");
                //实例化PHPExcel类（注意：实例化的时候前面需要加'\'）
                $objReader=new \PHPExcel_Reader_Excel5();
                $objPHPExcel = $objReader->load($path,$encode='utf-8');//获取excel文件
                $sheet = $objPHPExcel->getSheet(0); //激活当前的表
                $highestRow = $sheet->getHighestRow(); // 取得总行数
                $highestColumn = $sheet->getHighestColumn(); // 取得总列数
                $a=0;
                //将表格里面的数据循环到数组中
                if ($highestRow > 50) {$re= '测试版，导入数据量不得超过50！';die();}
                for($i=3;$i<=$highestRow;$i++)
                {
                    //*为什么$i=2? (因为Excel表格第一行应该是姓名，年龄，班级，从第二行开始，才是我们要的数据。)
                    $data[$a]['cid'] = $cid;//班级ID
                    $data[$a]['truename'] = $objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue();//姓名
                    $data[$a]['snumber'] = $objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();//xuehao
                    $data[$a]['pass'] = md5($objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue());
                    $data[$a]['class'] = $objPHPExcel->getActiveSheet()->getCell("D".$i)->getValue();
                    $data[$a]['grade'] = $objPHPExcel->getActiveSheet()->getCell("E".$i)->getValue();
                    
                     // 这里的数据根据自己表格里面有多少个字段自行决定
                    $a++;
                }
                //往数据库添加数据
                $stu = new Students;
                $res=$stu->saveAll($data);
                //$res = db('students')->insertAll($data);
                if($res){
                        //$this->success('操作成功！');
                        $re= 1;
                }else{
                        //$this->error('操作失败！');
                        $re= '插入数据库失败';
                   }
            }else{
                // 上传失败获取错误信息
                //$this->error($file->getError());
                $re= $file->getError();
            }
            return myjson($re,'','','');

        }
    }
























    
    
    
   
   
}
