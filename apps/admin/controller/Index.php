<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\controller\Siku;
class Index extends Siku
{
    public function index()
    {   

        $where['tid']=session('uid');
        $info=db('teachers')->where($where)->find();
        //dump($info);
        $this->assign('info',$info);
        return $this->fetch();
    }
     public function main()
    {
         $num['exam']=db('exams')->count();
         $num['te']=db('teachers')->count();
         $num['stu']=db('students')->count();
        $this->assign('num',$num);
        return $this->fetch();
    }
    function  tpass(){

        return $this->fetch();
    }
    function check_tpass(){
        $pass=request()->post('pass','','md5');
        $where['tid']=session('uid');
        $tpass=db('teachers')->where($where)->value('pass');
        if ($pass!=$tpass) {
            $re=0;
        } else {
            $re=1;
        }
        
         return $re;
    }
    function do_tpass(){
        $pass=request()->post('pass','','md5');
        $where['tid']=session('uid');
        $re=db('teachers')->where($where)->setField('pass',$pass);
        
        return $re;
    }
   
}
