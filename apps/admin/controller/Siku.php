<?php
namespace app\admin\controller;
use think\Controller;
use think\facade\Session;
class Siku extends Controller
{
	protected function _initialize()
	{
	//初始化的时候检查用户session是否过期
    	if($this->isLogin() === false) {
    		$this->error('您尚未登录或者登录已经过期，请重新登录',url('login/index'));
        		//$this->redirect('Login/index'); 
		}
	}
	/**
     * 检查用户是否登录
     * @return bool|mixed
     */
    public static function isLogin()
    {
        // 获取后台用户登录session
        $admin = session('username');
        // 若判断session值是否为空
        if (empty($admin)) {
            // 为空，无用户登录，返回false
            return false;
        } else {
            // 不为空，有用户登录，返回true
            return true;
        }

    }
	
}