<?php
namespace app\admin\model;
use think\Model;
class Questions extends Model
{
	//设置主键
	protected $pk = 'qid';
	//设置自动时间戳
	//protected $createTime = 'create_time';
	//设置自动转换类型
	protected $type = [
		'qid'=> 'integer',
		'qsubject'=>'integer',
		'qtype'=>'integer',
		//'kejie'=> 'float',
		//'birthday' => 'datetime',
		//'info'=> 'array',
	];
	// //自动完成
	// protected $auto = [];
	// protected $insert = ['pass'];
	// protected $update = [];
	// protected function setPassAttr($value)
	// {
	// return md5($value);
	// }
	// protected function setIpAttr()
	// 	{
	// 	return request()->ip();
	// 	}
	//获取器
	public function getQtitleAttr($value)
    {
        //$status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return htmlspecialchars_decode($value);
    }



	//一对多关联 一个题关联几个答案
	public function getanswers()
    {
        return $this->hasMany('Answers','qid');
    }
    public function getteacher()
    {
        return $this->belongsTo('Teachers','tid')->field('tid,uname as tname');
    }




}