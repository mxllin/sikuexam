<?php
namespace app\admin\model;
use think\Model;
class Answers extends Model
{
	//设置主键
	protected $pk = 'aid';
	//设置自动时间戳
	//protected $createTime = 'create_time';
	//设置自动转换类型
	protected $type = [
		'qid'=> 'integer',
		//'kejie'=> 'float',
		//'birthday' => 'datetime',
		//'info'=> 'array',
	];
	//获取器
	public function getAnswercontentAttr($value)
    {
        //$status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return htmlspecialchars_decode($value);
    }
	



}