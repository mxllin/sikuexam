<?php
namespace app\admin\model;
use think\Model;
class Exams extends Model
{
	//设置主键
	protected $pk = 'eid';
	//设置自动时间戳
	//protected $createTime = 'create_time';
	//设置自动转换类型
	protected $type = [
		'class'=> 'integer',
		'grade'=>'integer',
		//'admintype'=>'integer',
		//'kejie'=> 'float',
		//'birthday' => 'datetime',
		//'info'=> 'array',
	];
	//自动完成
	protected $auto = [];
	protected $insert = ['pass'];
	protected $update = [];
	protected function setPassAttr($value)
		{
		return md5($value);
		}
	// protected function setIpAttr()
	// 	{
	// 	return request()->ip();
	// 	}
	//修改器
	// public function setPassAttr($value)
 //    {
 //        return md5($value);
 //    }
		//一对多关联 一个题关联几个答案
	public function getstuanswers()
    {
        return $this->hasMany('app\index\model\StuAnswers','eid');
    }
    public function getmarkcount()
    {
        return $this->hasMany('app\index\model\StuAnswers','eid');
    }
    public function getAllocateAttr($value)
    {
        return json_decode($value,true);
    }



}