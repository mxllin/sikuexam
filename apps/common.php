<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**º¯Êý¿â
 * @Author: hxb0810
 /**
 * EXCEL 导入导出
 * auther: yuan
 * date:2016-10-31 17:05
 */
/**
+----------------------------------------------------------
 * Export Excel | 2013.08.23
 * Author:HongPing <hongping626@qq.com>
+----------------------------------------------------------
 * @param $expTitle     string File name
+----------------------------------------------------------
 * @param $expCellName  array  Column name
+----------------------------------------------------------
 * @param $expTableData array  Table data
+----------------------------------------------------------
 */

//表格导入
     function import(){
        if(request()->isPost()){
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public' .DS.'uploads'. DS . 'excel');
            if($info){
                //获取文件所在目录名
                $path=ROOT_PATH . 'public' . DS.'uploads'.DS .'excel/'.$info->getSaveName();
                //加载PHPExcel类
                vendor("PHPExcel.PHPExcel");
                //实例化PHPExcel类（注意：实例化的时候前面需要加'\'）
                $objReader=new \PHPExcel_Reader_Excel5();
                $objPHPExcel = $objReader->load($path,$encode='utf-8');//获取excel文件
                $sheet = $objPHPExcel->getSheet(0); //激活当前的表
                $highestRow = $sheet->getHighestRow(); // 取得总行数
                $highestColumn = $sheet->getHighestColumn(); // 取得总列数
                $a=0;
                //将表格里面的数据循环到数组中
                for($i=2;$i<=$highestRow;$i++)
                {
                    //*为什么$i=2? (因为Excel表格第一行应该是姓名，年龄，班级，从第二行开始，才是我们要的数据。)
                    $data[$a]['name'] = $objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue();//姓名
                    $data[$a]['age'] = $objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();//年龄
                    $data[$a]['class'] = $objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue();//班级
                     // 这里的数据根据自己表格里面有多少个字段自行决定
                    $a++;
                }
                //往数据库添加数据
                $res = Db::name('student')->insertAll($data);
                if($res){
                        $this->success('操作成功！');
                }else{
                        $this->error('操作失败！');
                   }
            }else{
                // 上传失败获取错误信息
                $this->error($file->getError());
            }
        }
    }

//表格导出处理
     function export(){
        //1.从数据库中取出数据
        $list = Db::name('student')->select();
        //2.加载PHPExcle类库
        vendor('PHPExcel.PHPExcel');
        //3.实例化PHPExcel类
        $objPHPExcel = new \PHPExcel();
        //4.激活当前的sheet表
        $objPHPExcel->setActiveSheetIndex(0);
        //5.设置表格头（即excel表格的第一行）
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')                      
                ->setCellValue('B1', '姓名')
                ->setCellValue('C1', '年龄')
                ->setCellValue('D1', '班级')
                ->setCellValue('E1', '电话')
                ->setCellValue('F1', '邮箱');
        //设置F列水平居中
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('F')->getAlignment()
                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //设置单元格宽度
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth(30); 
        //6.循环刚取出来的数组，将数据逐一添加到excel表格。
        for($i=0;$i<count($list);$i++){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($i+2),$list[$i]['id']);//添加ID
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($i+2),$list[$i]['name']);//添加姓名
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($i+2),$list[$i]['age']);//添加年龄
            $objPHPExcel->getActiveSheet()->setCellValue('D'.($i+2),$list[$i]['class']);//添加班级
            $objPHPExcel->getActiveSheet()->setCellValue('E'.($i+2),$list[$i]['tel']);//添加电话
            $objPHPExcel->getActiveSheet()->setCellValue('F'.($i+2),$list[$i]['email']);//添加邮箱
        }
        //7.设置保存的Excel表格名称
        $filename = '学生信息'.date('ymd',time()).'.xls';
        //8.设置当前激活的sheet表格名称；
        $objPHPExcel->getActiveSheet()->setTitle('学生信息');
        //9.设置浏览器窗口下载表格
        header("Content-Type: application/force-download");  
        header("Content-Type: application/octet-stream");  
        header("Content-Type: application/download");  
        header('Content-Disposition:inline;filename="'.$filename.'"');  
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;
    }
//转换为一维数组

// function i_array_column($input, $columnKey, $indexKey=null){
//     if(!function_exists('array_column')){ 
//         $columnKeyIsNumber  = (is_numeric($columnKey))?true:false; 
//         $indexKeyIsNull            = (is_null($indexKey))?true :false; 
//         $indexKeyIsNumber     = (is_numeric($indexKey))?true:false; 
//         $result                         = array(); 
//         foreach((array)$input as $key=>$row){ 
//             if($columnKeyIsNumber){ 
//                 $tmp= array_slice($row, $columnKey, 1); 
//                 $tmp= (is_array($tmp) && !empty($tmp))?current($tmp):null; 
//             }else{ 
//                 $tmp= isset($row[$columnKey])?$row[$columnKey]:null; 
//             } 
//             if(!$indexKeyIsNull){ 
//                 if($indexKeyIsNumber){ 
//                   $key = array_slice($row, $indexKey, 1); 
//                   $key = (is_array($key) && !empty($key))?current($key):null; 
//                   $key = is_null($key)?0:$key; 
//                 }else{ 
//                   $key = isset($row[$indexKey])?$row[$indexKey]:0; 
//                 } 
//             } 
//             $result[$key] = $tmp; 
//         } 
//         return $result; 
//     }else{
//         return array_column($input, $columnKey, $indexKey);
//     }
// }
//////判断是否md5
function is_md5($p){
  return preg_match("/^[a-z0-9]{32}$/",$p);
}
//分页函数
function getpage($table,$perpage){
    $l=D($table);
            $count      = $l->count();
            $Page       = new \Think\Page($count,$perpage);
            $Page->setConfig('prev',  '<span >ÉÏÒ»Ò³</span>');//ÉÏÒ»Ò³
            $Page->setConfig('next',  '<span >ÏÂÒ»Ò³</span>');//ÏÂÒ»Ò³
            $Page->setConfig('first', '<span >Ê×Ò³</span>');//µÚÒ»Ò³
            $Page->setConfig('last',  '<span >Î²Ò³</span>');//×îºóÒ»Ò³
            $show=$Page->show();// ·ÖÒ³ÏÔÊ¾Êä³ö
            $llist = $l->order('tid desc')->limit($Page->firstRow.','.$Page->listRows)->select();
            $data['page']=$show;
            $data['data']=$llist;
            return $data;
}
//学校函数
function getschool($schoolid){
    switch ($schoolid) {
      case '1':
        return '孝义中学';
        break;
      case '2':
        return '孝义二中';
        break;
      case '3':
        return '孝义三中';
        break;
      case '4':
        return '孝义四中';
        break;
      case '5':
        return '孝义五中';
        break;
      case '6':
        return '孝义实验中学';
        break;
      case '7':
        return '孝义华杰中学';
        break;
      case '8':
        return '孝义艺苑中学';
        break;
            case '0':
                return '教育局';
                break;
  }
}
//学校函数结束
//显示年级
function check_grade($grade){
    switch ($grade) { 
        case 1: 
            return '高一'; 
        break; 
        case 2: 
            return '高二';   
        break; 
        case 3: 
            return '高三';   
        break; 
    } 
}
//添加试题时候字符串和图片的处理，兼容word复制进来的内容
function question_str_img($qtitle){
    $title=htmlspecialchars_decode($qtitle);//实体转为字符
    if (stristr($title,'</p>')) {
        $title1=trim(str_replace('</p>','<br>',rtrim($title,'</p>')));//去掉最后一个段落给中间换行
        $title2=strip_tags($title1,'<img> <br> <u>');//去掉html标签，保留图片和换行
    } else {
        $title2=strip_tags($title,'<img> <br> <u>');//去掉html标签，保留图片和换行
    }
    if (substr($title2,-4)==='<br>') {
        $title3=substr($title2, 0,strlen($title2)-4);
    } else {
        $title3=$title2;
    }
    
    return $title3;
}
//添加试题答案字符串和图片的处理，兼容word复制进来的内容
function answer_str_img($answer){
    $title=htmlspecialchars_decode($answer);//实体转为字符
    if (stristr($title,'</p>')) {
        $title1=trim(str_replace('</p>','<br>',rtrim($title,'</p>')));//去掉最后一个段落给中间换行
        $title2=preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", " ",strip_tags($title1,'<img> <br>'));//去掉html标签，保留图片和换行
    } else {
        $title2=preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", " ",strip_tags($title,'<img> <br>'));//去掉html标签，保留图片和换行
    }
    return $title2;
}
//添加试题到考试时处理试题ID的字符串
function add_question_str($qids,$newqids){
    $qids.=','.$newqids;
    $re=array_unique(explode(',',ltrim($qids,',')));
    return implode(',', $re);

}

//删除试题时候字符串处理
function del_question_str($qids,$qid){
    //$a='a,b,c,d';
        $a=explode(',', $qids);
        $b=array_diff($a, [$qid]);
        return implode(',',$b);
}



//获取权限函数
function get_adminType($admintype){
        switch ($admintype) {
            case '-1':
                return '游客';
                break;
            case '1':
                return '教师';
                break;
            case '2':
                return '超级管理员';
                break;
            case '3':
                return '宇宙大帝';
                break;
    }
}
//获取题目的学科
//获取权限函数
function get_question_subject($type){
        switch ($type) {
            case 1:
                return '语文';
                break;
            case 2:
                return '数学';
                break;
            case 3:
                return '外语';
                break;
            case 4:
                return '物理';
                break;
            case 5:
                return '化学';
                break;
            case 6:
                return '生物';
                break;
            case 7:
                return '政治';
                break;
            case 8:
                return '历史';
                break;
            case 9:
                return '地理';
                break;
    }
}
function get_question_type($type){
        switch ($type) {
            case 1:
                return '单选';
                break;
            case 2:
                return '多选';
                break;
            case 3:
                return '填空';
                break;
            case 4:
                return '简答';
                break;
    }
}
//验证复选框是否选中
function check_checkbox($value,$arr){
    if(in_array($value, $arr)){ 
        echo 'checked="checked"';
        } 

}
//字符串转数组
function ch2arr($str)
{
    $length = mb_strlen($str, 'utf-8');
    //$array = [];
    for ($i=0; $i<$length; $i++)  
        $array[] = mb_substr($str, $i, 1, 'utf-8');    
    return $array;
}
//获取各个选课项目的百分比
function get_percent($seid,$schoolid){
    $re=M('sel')->where('seid='.$seid)->find();
    $where['seid']=$seid;
    $map['year']=$where['year']=$re['year'];
    $map['schoolid']=$re['schoolid'];
    if (!empty($schoolid)) {
        $where['schoolid']=$schoolid;
    }
    $countstu=M('stu')->where($map)->count();
    //dump ($countstu);
    $countstusel=M('selstu')->where($where)->count();
    //dump($countstusel);
    $a=intval($countstu);
    $b=intval($countstusel);
    //echo $a;
    $res=($b/$a)*100;
    //echo $res;
    
    $data['title']= $countstusel.'/'.$countstu.'='.$res.'%';
    $data['percent'] =$res.'%';
    return $data;

}
//生成适合layui的数据格式
function myjson($code,$msg="",$count,$data=array()){
  $result=array(
   'code'=>$code,
   'msg'=>$msg,
   'count'=>$count,
   'data'=>$data
  );
  //输出json
  echo json_encode($result);
  exit;
}
/**
 * 字符串转化为数组，支持中英文逗号空格
 *
 * @param     string  $strs  带有特殊符号的字符串
 * @return    int
 */
function strsToArray($strs) {
	$result = array();
	$array = array();
	$strs = str_replace('，', ',', $strs);
	$strs = str_replace(' ', ',', $strs);
	$array = explode(',', $strs);
	foreach ($array as $key => $value) {
		if ('' != ($value = trim($value))) {
			$result[] = $value;
		}
	}
	return $result;
}

//转化为一位数组
function i_array_column($input, $columnKey, $indexKey=null){
    if(!function_exists('array_column')){ 
        $columnKeyIsNumber  = (is_numeric($columnKey))?true:false; 
        $indexKeyIsNull            = (is_null($indexKey))?true :false; 
        $indexKeyIsNumber     = (is_numeric($indexKey))?true:false; 
        $result                         = array(); 
        foreach((array)$input as $key=>$row){ 
            if($columnKeyIsNumber){ 
                $tmp= array_slice($row, $columnKey, 1); 
                $tmp= (is_array($tmp) && !empty($tmp))?current($tmp):null; 
            }else{ 
                $tmp= isset($row[$columnKey])?$row[$columnKey]:null; 
            } 
            if(!$indexKeyIsNull){ 
                if($indexKeyIsNumber){ 
                  $key = array_slice($row, $indexKey, 1); 
                  $key = (is_array($key) && !empty($key))?current($key):null; 
                  $key = is_null($key)?0:$key; 
                }else{ 
                  $key = isset($row[$indexKey])?$row[$indexKey]:0; 
                } 
            } 
            $result[$key] = $tmp; 
        } 
        return $result; 
    }else{
        return array_column($input, $columnKey, $indexKey);
    }
}

//学生信息修改时edit初始化选择班级名称
function get_class_name($cid,$classes){
    foreach ($classes as $key => $value) {
        if ($value['cid']==$cid) {
            return $value['title'];
        }
    }

}
//计算二维数组中子数组第一个值的和
function array_first_sum($arr){
    $sum=0;
    foreach ($arr as $v) {
        $sum+=$v[0];
    }
    return $sum;
}


?>