// $(function () {
//     //加载弹出层
//     layui.use(['jquery','form','element'],
//     function() {
//         layer = layui.layer;
//         element = layui.element;
//         $ = layui.jquery;
//     });    
    
// })
/*弹出层*/
/*
    参数解释：
    title   标题
    url     请求的url
    id      需要操作的数据id
    w       弹出层宽度（缺省调默认值）
    h       弹出层高度（缺省调默认值）
*/
function x_admin_show(title,url,w,h){
    if (title == null || title == '') {
        title=false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '') {
        w=($(window).width()*0.9);
    };
    if (h == null || h == '') {
        h=($(window).height() - 50);
    };
    layer.open({
        type: 2,
        area: [w+'px', h +'px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade:0.4,
        title: title,
        content: url
    });
}

/*关闭父级弹出框口*/
function x_father_close(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
	//setTimeout('parent.layer.close(index)',5000);
}
//显示课节
function check_kejie(ke){
    switch (ke) { 
        case 1: 
            return '第一节'; 
        case 2: 
            return '第二节';   
        break; 
        case 3: 
            return '第三节';   
        break; 
        case 4: 
            return '第四节'; 
        break; 
        case 5: 
            return '第五节'; 
        break; 
        case 6: 
            return '第六节';   
        break; 
        case 7: 
            return '第七节'; 
        break; 
        case 8: 
            return '自习一'; 
        break; 
        case 9: 
            return '自习二';   
        break; 
        case 10: 
            return '自习三';   
        break; 
    } 
} 
//显示年级
function check_grade(g){
    switch (g) { 
        case 1: 
            return '高一'; 
        break; 
        case 2: 
            return '高二';   
        break; 
        case 3: 
            return '高三';   
        break; 
    } 
} 
//获取编辑模板类型
function check_edittype(g){
    switch (g) { 
        case 1: 
            return 'select'; 
        break; 
        case 2: 
            return 'select';   
        break; 
        case 3: 
            return 'tiankong';   
        break;
        case 4: 
            return 'jianda';   
        break; 
        case 5: 
            return 'title';   
        break;  
    } 
} 
//显示权限
function check_admin(g){
    switch (g) { 
        case 1: 
            return '教师'; 
        break; 
        case 2: 
            return '超级管理员';   
        break; 
        case 3: 
            return '宇宙大帝';   
        break; 
    } 
} 
//显示转团办理状态
function check_tuanstatus(ke){
    if (ke==1) {
      return '<button class="layui-btn layui-btn-xs layui-btn-normal">已办理</button>'; 
    } else {
      return '<button class="layui-btn layui-btn-xs layui-btn-danger">未办理</button>';
    } 
} 
//加强是否补办样式
function check_tuanzheng(z){
  if (z=='是') {
    return '<b style="color:#FF5722;">是</b>';
  }else{
    return '否';
  }
}
//
function get_img_url(title,data){
    data=data.substr(0, data.length-1);
    
    if(data.indexOf(",")>-1){
        var urls = [],iurls=[],imd=[];
        urls=data.split(",");
        //return urls;
        if (urls.length > 0) {
            for (var i = 0; i < urls.length; i++) {
             iurls[i]='../../../public/uploads/tingkeimg/' + urls[i];
             imd[i]={
                'alt':title,'pid':i+1,'src':iurls[i],'thumb':''
             }
             // imd['alt']=title;
             // imd['pid']=i+1;
             // imd['src']=iurls[i];
             // imd['thumb']='';
             //console(iurls[i]);
             //return '<div id="imgbox"></div>';
            }
            //console(iurls);
            //return iurls;
            return imd;
        }           
    }else{
        var imd=[{
                'alt':title,'pid':i+1,'src':'../../../public/uploads/tingkeimg/' + data ,'thumb':''
             }]
        //return '../../../public/uploads/tingkeimg/' + data ;
        //return data;
        return imd;
    }
}

//返回layui.photo需要的数据格式
// function get_img_data(title,imgurl){
//     var imgdata=[];
//     imgdata['title']=title;
//     imgdata['id']='123';
//     imgdata['start']=0;
//     imgdata['data']['alt']=title;
//     imgdata['data']['pid']='666';
//     imgdata['data']['src']=imgurl;
//     imgdata['data']['thumb']='';
//     return imgdata;
// }
function get_img_data(title,imgurl){
    var imgdata=
        {
      "title": title, //相册标题
      "id": 123, //相册id
      "start": 0, //初始显示的图片序号，默认0
       "data":imgurl
      // "data": [   //相册包含的图片，数组格式
      //   {
      //     "alt": "图片名",
      //     "pid": 666, //图片id
      //     "src": imgurl, //原图地址
      //     "thumb": "" //缩略图地址
      //   }
      // ]
    }
    //return JSON.stringify(imgdata);
    return imgdata;
}
//计算时长函数
function get_DateHours(date1,date2){//date1:小日期   date2:大日期
  　　var sdate = new Date(date1); 
  　　var now = new Date(date2); 
  　　var days = now.getTime() - sdate.getTime(); 
  　　var hours = parseInt(days / (1000 * 60 * 60)); 
  　　return sdate; 
    // if (hours >= 24) {
    //   var day = parseInt(hours / 24);
    //   var hour = hours-day*24;
    //   return day+'天'+hour+'小时';
    // } else {
    //   return hours+'小时';
    // }
  }
function  getDaysBetween(dateString1,dateString2){
   var  startDate = Date.parse(dateString1);
   var  endDate = Date.parse(dateString2);
   var hours=parseInt((endDate - startDate)/(60*60*1000));
   // alert(days);
   if (hours < 24) {
    return hours+'小时';
    }else{
      var day = parseInt(hours / 24);
      var hour =parseInt(hours-day*24);
      if (hour === 0 ) {
        return day+'天';
      }else{
        return day+'天'+hour+'小时';
      }
    }
}


function get_question_subject($type){
        switch ($type) {
            case 1:
                return '<span class="layui-badge layui-bg-blue">语文</span>';
                break;
            case 2:
                return '<span class="layui-badge layui-bg-blue">数学</span>';
                break;
            case 3:
                return '<span class="layui-badge layui-bg-blue">外语</span>';
                break;
            case 4:
                return '<span class="layui-badge layui-bg-blue">物理</span>';
                break;
            case 5:
                return '<span class="layui-badge layui-bg-blue">化学</span>';
                break;
            case 6:
                return '<span class="layui-badge layui-bg-blue">生物</span>';
                break;
            case 7:
                return '<span class="layui-badge layui-bg-blue">政治</span>';
                break;
            case 8:
                return '<span class="layui-badge layui-bg-blue">历史</span>';
                break;
            case 9:
                return '<span class="layui-badge layui-bg-blue">地理</span>';
                break;
    }
}
function get_question_type($type){
        switch ($type) {
            case 1:
                return '<span class="layui-badge">单选</span>';
                break;
            case 2:
                return '<span class="layui-badge layui-bg-orange">多选</span>';
                break;
            case 3:
                return '<span class="layui-badge layui-bg-green">填空</span>';
                break;
            case 4:
                return '<span class="layui-badge layui-bg-cyan">简答</span>';
                break;
            case 5:
                return '<span class="layui-badge layui-bg-gray">标题</span>';
                break;
    }
}

function get_answers(d){
  if (d.qtype < 4) {
    var ht='';
      for (var i=0;i<d.getanswers.length;i++){ 
        
        ht+= '<div class="layui-col-md6 layui-col-xs12 layui-col-sm12">'+d.getanswers[i].answercontent+'</div>';
        
      }
      return ht;
  } else {
    return '';
  }
}

function get_right_answer2($d){//旧的答案显示函数显示具体答案
  if ($d.qtype < 4) {
      var rights='';
      for (var i = 0; i < $d.getanswers.length; i++) {
        //console.log(typeof($d.getanswers[i].answercontent));
        //循环输出答案列表

        if ($d.getanswers[i].isright==1){
          
          rights+='&nbsp;&nbsp;'+$d.getanswers[i].answercontent+'&nbsp;&nbsp;';
        }
      }
      return '<p><span style="color:#FF5722">正确答案:&nbsp;&nbsp;'+rights+'</span></p>';
  } else if ($d.qtype == 5) {
      return '';
  } else {
      return '<p><span style="color:#FF5722">简答题需人工阅卷，无标准答案。</span></p>';
  } 
}

function get_right_answer(d){
  if (d.qtype < 3) {
      return '<p><span style="color:#FF5722">正确答案：&nbsp;&nbsp;'+d.rightselect+'</span></p>';
  } else if (d.qtype == 5) {
      return '';
  } else {
      return '<p><span style="color:#FF5722">正确答案：&nbsp;&nbsp;'+d.rightselect+'&nbsp;（此题需人工阅卷）</span></p>';
  } 
}



